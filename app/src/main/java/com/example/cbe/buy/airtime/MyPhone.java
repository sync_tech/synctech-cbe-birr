package com.example.cbe.buy.airtime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.models.User;
import com.example.cbe.helpers.Utils;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;

public class MyPhone extends AppCompatActivity {

    private EditText amount, pin;
    private Button send;
    private TextInputLayout layoutAmount, layoutPin;
    private BottomSheetDialog mBottomSheetDialog;
    View bottomSheetLayout;
    TextView title, content;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_phone);

        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        View bottomSheet = findViewById(R.id.framelayout_bottom_sheet);

        amount = findViewById(R.id.edit_amout);
        pin = findViewById(R.id.edit_pin);
        send = findViewById(R.id.btn_Send);

        layoutAmount = findViewById(R.id.layout_amount);
        layoutPin = findViewById(R.id.layout_pin);
        bottomSheetLayout = getLayoutInflater().inflate(R.layout.bottom_sheet_dialog, null);
        title = bottomSheetLayout.findViewById(R.id.tv_title);
        content = bottomSheetLayout.findViewById(R.id.tv_detail);

        user = new User();


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();
            }
        });
    }
    private void validation() {

        if (amount.getText() == null || amount.getText().toString().equals("")) {
            Toast.makeText(MyPhone.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutAmount);
            layoutAmount.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(MyPhone.this, "Please Enter PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }
        setmBottomSheetDialog();
    }
    private void sendData(){

        String message = "*847*3*1*" + amount.getText().toString()+"*"+pin.getText().toString() + "*1#" ;
        Utils.sendUSSD(message, MyPhone.this, MyPhone.this);

    }
    private void setmBottomSheetDialog(){
        title.setText("Confirmation");
        content.setText("Are you sure you want to buy airtime of " + amount.getText().toString()+" to "+pin.getText().toString());
        (bottomSheetLayout.findViewById(R.id.button_cancel)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        (bottomSheetLayout.findViewById(R.id.button_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(bottomSheetLayout);
        mBottomSheetDialog.show();
    }
}
