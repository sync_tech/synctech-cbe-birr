package com.example.cbe;

import com.example.cbe.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi  {

    @POST("/registerUser")
    Call<User> createUser(@Body User user);

}
