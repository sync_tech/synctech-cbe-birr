package com.example.cbe.models;

import com.orm.SugarRecord;

public class Beneficiary extends SugarRecord {
    String phone;
    String inputReference;
    
    public Beneficiary(String phone, String inputReference) {
        this.phone = phone;
        this.inputReference = inputReference;
    }
    public Beneficiary(){

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInputReference() {
        return inputReference;
    }

    public void setInputReference(String inputReference) {
        this.inputReference = inputReference;
    }
}
