package com.example.cbe;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.cbe.helpers.TypefaceUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.example.cbe.Constant.*;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Splash extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);
        ImageView splashImage = findViewById(R.id.splash_image);

        TypefaceUtil.returnToDefault();
        //creat instance of firebase




        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String thetoken = task.getResult().getToken();

                        // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
                        Constant.token = task.getResult().getToken();
//                        Toast.makeText(getApplicationContext(), thetoken, Toast.LENGTH_SHORT).show();

                    }
                });





        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {


                Boolean Registered;
                final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Splash.this);
                Registered = sharedPref.getBoolean("Registered", false);

                if (Registered)
                {
                    startActivity(new Intent(Splash.this,Home.class));
                    Splash.this.finish();
                }else {
                    startActivity(new Intent(Splash.this,Signup.class));
                    Splash.this.finish();
                }


            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
