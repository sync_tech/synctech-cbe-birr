package com.example.cbe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.cbe.R;
import com.example.cbe.beneficiaries.BeneficiaryActivity;
import com.example.cbe.buy.airtime.BuyAirtime;
import com.example.cbe.buy.airtime.MyPhone;
import com.example.cbe.cashout.CashOut;
import com.example.cbe.helpers.Utils;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private CardView sendMoney, cashOut, buyAirtime, buyGoods, payBills, balance, others;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //toolbar
        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        sendMoney = findViewById(R.id.cardView_send_money);
        cashOut = findViewById(R.id.cardView_cash_out);
        buyAirtime = findViewById(R.id.cardView_buy_airtime);
        buyGoods = findViewById(R.id.cardView_buy_goods);
        payBills = findViewById(R.id.cardView_pay_bills);
        balance = findViewById(R.id.cardView_balance);
        others = findViewById(R.id.cardView_others);

        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, SendMoney.class);
                startActivity(intent);

            }
        });
        cashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, CashOut.class);
                startActivity(intent);
            }
        });
        buyAirtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, BuyAirtime.class);
                startActivity(intent);
            }
        });
        buyGoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, BuyGoods.class);
                startActivity(intent);
            }
        });
        payBills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, PayBills.class);
                startActivity(intent);
            }
        });
        balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Balance.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.nav_about) {
            Intent intent = new Intent(Home.this, AboutActivity.class);
            startActivity(intent);
        }
        if (id == R.id.nav_balance) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter PIN");
            View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
            builder.setView(customLayout);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // send data from the AlertDialog to the Activity
                    EditText editText = customLayout.findViewById(R.id.edit_pin);
                    String pinText = editText.getText().toString();
                    String message = "*847*6*" + pinText + "#" ;
                    Utils.sendUSSD(message, Home.this, Home.this);

                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        }
        if (id == R.id.nav_pin) {
            Intent intent = new Intent(Home.this, ChangePin.class);
            startActivity(intent);
        }
        if (id == R.id.nav_help) {
            Intent intent = new Intent(Home.this, Signup.class);
            startActivity(intent);
        }
        if (id == R.id.nav_statement) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Enter PIN");
                View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
                builder.setView(customLayout);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // send data from the AlertDialog to the Activity
                        EditText editText = customLayout.findViewById(R.id.edit_pin);
                        String pinText = editText.getText().toString();
                        String message = "*847*7*1*" + pinText + "#" ;
                        Utils.sendUSSD(message, Home.this, Home.this);

                    }
                });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

                AlertDialog dialog = builder.create();
                dialog.show();



        }
        if (id == R.id.nav_manage_beneficiary) {
            Intent intent = new Intent(Home.this, BeneficiaryActivity.class);
            startActivity(intent);
        }
        if (id == R.id.nav_secretword) {
            Intent intent = new Intent(Home.this, SecretWordActivity.class);
            startActivity(intent);
        }
        if (id == R.id.nav_share) {
            shareWith();
        }
        if (id == R.id.nav_logout) {
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();

                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }



        return true;
    }
    public void shareWith() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Check out CBE Birr " + "https://#";
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "share_via"));
    }
}
