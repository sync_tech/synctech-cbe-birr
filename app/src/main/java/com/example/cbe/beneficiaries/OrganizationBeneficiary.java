package com.example.cbe.beneficiaries;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.helpers.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

public class OrganizationBeneficiary extends Fragment {

    private FloatingActionButton fab;
    private TextView noDataContainerText;
    private RecyclerView recyclerView;
    private RelativeLayout noDataContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.organization_beneficiary_fragment, null);


        fab = view.findViewById(R.id.add_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getActivity(), "test", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), AddOrganizationBeneficiary.class);
                startActivity(intent);

            }
        });

        recyclerView = view.findViewById(R.id.my_recycler_view);
        noDataContainer = view.findViewById(R.id.no_data_container);
        noDataContainerText =view.findViewById(R.id.no_data_container_text);
        noDataContainer.setVisibility(View.GONE);


        return view;
    }


}