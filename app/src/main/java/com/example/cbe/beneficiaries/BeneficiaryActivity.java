package com.example.cbe.beneficiaries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.cbe.R;
import com.example.cbe.cashout.ShortCodeFragment;
import com.example.cbe.cashout.TabAdapter;
import com.example.cbe.cashout.TillNoFragment;
import com.google.android.material.tabs.TabLayout;

public class BeneficiaryActivity extends AppCompatActivity {

    private BeneficiaryTabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary);
        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new BeneficiaryTabAdapter(getSupportFragmentManager());
        adapter.addFragment(new CustomerBeneficiary(), "Customer  Beneficiary");
        adapter.addFragment(new OrganizationBeneficiary(), "Organization Beneficiary");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


    }
}
