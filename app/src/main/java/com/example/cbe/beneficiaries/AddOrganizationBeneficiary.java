package com.example.cbe.beneficiaries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.helpers.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

public class AddOrganizationBeneficiary extends AppCompatActivity {

    private EditText shortCode, pin, reference;
    private Button save;
    private TextInputLayout layoutShortCode, layoutPin, layoutReference;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_organization_beneficiary);
        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        shortCode = findViewById(R.id.edit_shortcode);
        pin = findViewById(R.id.edit_pin);
        reference = findViewById(R.id.edit_reference);
        save = findViewById(R.id.btn_save);

        layoutShortCode = findViewById(R.id.layout_shortcode);
        layoutPin = findViewById(R.id.layout_pin);
        layoutReference = findViewById(R.id.layout_reference);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();
            }
        });







    }
    private void validation() {

        if (shortCode.getText() == null || shortCode.getText().toString().equals("")) {
            Toast.makeText(AddOrganizationBeneficiary.this, "Please Enter Short Code", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutShortCode);
            layoutShortCode.requestFocus();
            return;
        }
        if (reference.getText() == null || reference.getText().toString().equals("")) {
            Toast.makeText(AddOrganizationBeneficiary.this, "Please Enter Reference", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutReference);
            layoutReference.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(AddOrganizationBeneficiary.this, "Please Enter PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }
        sendData();
    }

    private void sendData() {

        Utils.sendUSSD("*804#", AddOrganizationBeneficiary.this, AddOrganizationBeneficiary.this);

    }

}
