package com.example.cbe.beneficiaries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.helpers.Utils;
import com.example.cbe.models.Beneficiary;
import com.google.android.material.textfield.TextInputLayout;

public class AddCustomerBeneficiary extends AppCompatActivity {
    private EditText phone, reference, pin;
    private Button send;
    private TextInputLayout layoutPhone, layoutPin, layoutReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer_beneficiary);
        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        phone = findViewById(R.id.edit_phone);
        pin = findViewById(R.id.edit_pin);
        reference = findViewById(R.id.edit_reference);
        send = findViewById(R.id.btn_save);

        layoutPhone = findViewById(R.id.layout_phone);
        layoutPin = findViewById(R.id.layout_pin);
        layoutReference = findViewById(R.id.layout_reference);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();
            }
        });


    }

    private void validation() {

        if (phone.getText() == null || phone.getText().toString().equals("")) {
            Toast.makeText(AddCustomerBeneficiary.this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPhone);
            layoutPhone.requestFocus();
            return;
        }
        if (reference.getText() == null || reference.getText().toString().equals("")) {
            Toast.makeText(AddCustomerBeneficiary.this, "Please Enter Reference", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutReference);
            layoutReference.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(AddCustomerBeneficiary.this, "Please Enter PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }

        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setPhone(phone.getText().toString());
        beneficiary.setInputReference(reference.getText().toString());

        beneficiary.save();

        sendData();
    }

    private void sendData() {

        Utils.sendUSSD("*804#", AddCustomerBeneficiary.this, AddCustomerBeneficiary.this);

    }
}
