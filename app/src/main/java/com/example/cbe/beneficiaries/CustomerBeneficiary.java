package com.example.cbe.beneficiaries;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.helpers.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

public class CustomerBeneficiary extends Fragment {

    FloatingActionButton fab;
    TextView noDataContainerText;
    private RecyclerView recyclerView;
    private RelativeLayout noDataContainer;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.customer_beneficiary_fragment, null);

        fab = view.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AddCustomerBeneficiary.class);
                startActivity(intent);
            }
        });

        recyclerView = view.findViewById(R.id.my_recycler_view);
        noDataContainer = view.findViewById(R.id.no_data_container);
        noDataContainerText =view.findViewById(R.id.no_data_container_text);
        //noDataContainer.setVisibility(View.GONE);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new Thread(() -> {


            recyclerView = view.findViewById(R.id.my_recycler_view);
            noDataContainer = view.findViewById(R.id.no_data_container);
            noDataContainerText =view.findViewById(R.id.no_data_container_text);
            //noDataContainer.setVisibility(View.GONE);
//            adapter = new IncomeSourceAdapter(this, incomeSourceList);
//            recyclerView.setAdapter(adapter);
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
//            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);

            if (recyclerView.getAdapter() != null) {
                if (recyclerView.getAdapter().getItemCount() == 0) {
                    noDataContainer.setVisibility(View.VISIBLE);
                } else if (recyclerView.getAdapter().getItemCount() != 0) {
                    noDataContainer.setVisibility(View.GONE);
                }
            } else {
                noDataContainer.setVisibility(View.VISIBLE);
            }
    }).start();
    }
}