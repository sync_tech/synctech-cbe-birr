package com.example.cbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.cbe.helpers.Utils;

public class PayBills extends AppCompatActivity {

    CardView shortCode, beneficiaryShortcode, derash, ethiotelecomBill;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_bills);

        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());


        shortCode = findViewById(R.id.cardView_shortCode);
        beneficiaryShortcode = findViewById(R.id.cardView_beneficiaryCode);
        derash = findViewById(R.id.cardView_derash);
        ethiotelecomBill = findViewById(R.id.cardView_ethiotelecom);

        shortCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = "*847*5*" + "2#" ;
                Utils.sendUSSD(message, PayBills.this, PayBills.this);
            }
        });
        beneficiaryShortcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "*847*5*" + "1#" ;
                Utils.sendUSSD(message, PayBills.this, PayBills.this);
            }
        });

        derash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "*847*5*" + "3#" ;
                Utils.sendUSSD(message, PayBills.this, PayBills.this);
            }
        });

        ethiotelecomBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "*847*5*" + "4#" ;
                Utils.sendUSSD(message, PayBills.this, PayBills.this);
            }
        });




    }
}
