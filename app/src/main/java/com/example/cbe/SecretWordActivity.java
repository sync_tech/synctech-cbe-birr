package com.example.cbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.helpers.Utils;
import com.google.android.material.textfield.TextInputLayout;

public class SecretWordActivity extends AppCompatActivity {

    private EditText newSecretWord, confirmSecretWord, pin;
    private Button change;
    private TextInputLayout layoutNewSecretWord, layoutConfirmSecretWord, layoutPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secret_word);
        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        newSecretWord = findViewById(R.id.edit_newSecretWord);
        confirmSecretWord = findViewById(R.id.edit_confirmSecretWord);
        pin = findViewById(R.id.edit_pin);
        change = findViewById(R.id.btn_change);

        layoutNewSecretWord = findViewById(R.id.layout_newSecretWord);
        layoutConfirmSecretWord = findViewById(R.id.layout_confirmSecretWord);
        layoutPin = findViewById(R.id.layout_pin);


        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });


    }

    private void validation() {

        if (newSecretWord.getText() == null || newSecretWord.getText().toString().equals("")) {
            Toast.makeText(SecretWordActivity.this, "Please Enter New Secret Word", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutNewSecretWord);
            layoutNewSecretWord.requestFocus();
            return;
        }
        if (confirmSecretWord.getText() == null || confirmSecretWord.getText().toString().equals("")) {
            Toast.makeText(SecretWordActivity.this, "Please Confirm Your New Secret Word", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutConfirmSecretWord);
            layoutConfirmSecretWord.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(SecretWordActivity.this, "Please Enter Your PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }
        if (!newSecretWord.getText().toString().equals(confirmSecretWord.getText().toString())) {
            Toast.makeText(SecretWordActivity.this, "Your Secret Word Does not Match!", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutConfirmSecretWord);
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutNewSecretWord);
            layoutConfirmSecretWord.requestFocus();
            return;
        }
        sendData();
    }

    private void sendData() {

        String message = "*847*7*4*" + newSecretWord.getText().toString() + "*" + confirmSecretWord.getText().toString() + "*" + pin.getText().toString() + "#";
        Utils.sendUSSD(message, SecretWordActivity.this, SecretWordActivity.this);

    }
}
