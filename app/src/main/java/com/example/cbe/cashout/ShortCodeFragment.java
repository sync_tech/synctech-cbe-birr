package com.example.cbe.cashout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.R;
import com.example.cbe.helpers.Utils;
import com.google.android.material.textfield.TextInputLayout;

public class ShortCodeFragment extends Fragment {

    private EditText amount, pin, shortCode;
    private Button send;
    private TextInputLayout layoutAmount, layoutPin, layoutShortCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.shortcode_fragment, null);

        amount = view.findViewById(R.id.edit_amout);
        pin = view.findViewById(R.id.edit_pin);
        shortCode = view.findViewById(R.id.edit_shortcode);
        send = view.findViewById(R.id.btn_Send);

        layoutAmount = view.findViewById(R.id.layout_amount);
        layoutPin = view.findViewById(R.id.layout_pin);
        layoutShortCode = view.findViewById(R.id.layout_shortcode);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();
            }
        });


        return view;
    }
    private void validation() {

        if (shortCode.getText() == null || shortCode.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Please Enter Short Code", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutShortCode);
            layoutShortCode.requestFocus();
            return;
        }
        if (amount.getText() == null || amount.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutAmount);
            layoutAmount.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Please Enter PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }
        sendData();
    }
    private void sendData(){

        Utils.sendUSSD("*804#", getActivity(), getActivity());

    }
}