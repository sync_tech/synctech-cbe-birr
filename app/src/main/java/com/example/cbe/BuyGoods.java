package com.example.cbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.helpers.Utils;
import com.google.android.material.textfield.TextInputLayout;

public class BuyGoods extends AppCompatActivity {

    private EditText tillNo, amount, pin;
    private Button send;
    private TextInputLayout layoutAmount, layoutPin, layoutTillNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_goods);

        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());


        amount = findViewById(R.id.edit_amout);
        pin = findViewById(R.id.edit_pin);
        tillNo = findViewById(R.id.edit_tillNo);
        send = findViewById(R.id.btn_Send);

        layoutAmount = findViewById(R.id.layout_amount);
        layoutPin = findViewById(R.id.layout_pin);
        layoutTillNo = findViewById(R.id.layout_tillNo);



        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
    }
    private void validation() {

        if (tillNo.getText() == null || tillNo.getText().toString().equals("")) {
            Toast.makeText(BuyGoods.this, "Please Enter Till Number", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutTillNo);
            layoutTillNo.requestFocus();
            return;
        }
        if (amount.getText() == null || amount.getText().toString().equals("")) {
            Toast.makeText(BuyGoods.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutAmount);
            layoutAmount.requestFocus();
            return;
        }
        if (pin.getText() == null || pin.getText().toString().equals("")) {
            Toast.makeText(BuyGoods.this, "Please Enter PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutPin);
            layoutPin.requestFocus();
            return;
        }
        sendData();
    }
    private void sendData(){

        Utils.sendUSSD("*804#", BuyGoods.this, BuyGoods.this);

    }
}
