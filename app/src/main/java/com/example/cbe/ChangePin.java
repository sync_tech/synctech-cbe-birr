package com.example.cbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.buy.airtime.OtherPhone;
import com.example.cbe.helpers.Utils;
import com.google.android.material.textfield.TextInputLayout;

public class ChangePin extends AppCompatActivity {

    private EditText oldPin, newPin, confirmPin;
    private Button change;
    private TextInputLayout layoutOldPin, layoutNewPin, layoutConfirmPin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());


        oldPin = findViewById(R.id.edit_oldpin);
        newPin = findViewById(R.id.edit_newpin);
        confirmPin = findViewById(R.id.edit_confirm_pin);
        change = findViewById(R.id.btn_change);

        layoutOldPin = findViewById(R.id.layout_oldpin);
        layoutNewPin = findViewById(R.id.layout_newpin);
        layoutConfirmPin = findViewById(R.id.layout_confirm_pin);


        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });


    }
    private void validation() {

        if (oldPin.getText() == null || oldPin.getText().toString().equals("")) {
            Toast.makeText(ChangePin.this, "Please Enter Old PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutOldPin);
            layoutOldPin.requestFocus();
            return;
        }
        if (newPin.getText() == null || newPin.getText().toString().equals("")) {
            Toast.makeText(ChangePin.this, "Please Enter New PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutNewPin);
            layoutNewPin.requestFocus();
            return;
        }
        if (confirmPin.getText() == null || confirmPin.getText().toString().equals("")) {
            Toast.makeText(ChangePin.this, "Please Confirm Your New PIN", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutConfirmPin);
            layoutConfirmPin.requestFocus();
            return;
        }
        if (!newPin.getText().toString().equals(confirmPin.getText().toString())) {
            Toast.makeText(ChangePin.this, "Your PIN Does not Match!", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutConfirmPin);
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(layoutNewPin);
            layoutConfirmPin.requestFocus();
            return;
        }
        sendData();
    }
    private void sendData(){

        String message = "*847*7*2*" + oldPin.getText().toString()+"*" + newPin.getText().toString()+"*"+confirmPin.getText().toString() + "#" ;
        Utils.sendUSSD(message, ChangePin.this, ChangePin.this);

    }
}
