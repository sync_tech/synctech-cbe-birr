package com.example.cbe;

import android.annotation.SuppressLint;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.cbe.helpers.Utils;
import com.example.cbe.models.User;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.cbe.Constant.*;

public class Signup extends AppCompatActivity {


    TextInputLayout Lfname,LfathersName, LgrandFatherName, LmotherName,
            LdateOfBirth, LidCode, LissuedBy, LexpireDate, LcustomerPhoneNumber;
    AppCompatEditText firstName, fatherName, grandFatherName, motherName,
            dateOfBirth, idCode, issuedBy, expireDate, customerPhoneNumber;
    AppCompatButton Signup;
    Spinner spinnerID, spinnerGender;
    Calendar calendar;
    android.app.DatePickerDialog picker;
    Date DOB, DOE;
    String gender, identification;
    public static ApiManager apiManager;
    User user;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        apiManager = ApiManager.getInstance();

        //layout
        Lfname = findViewById(R.id.layout_fname);
        LfathersName = findViewById(R.id.layout_fathersName);
        LgrandFatherName = findViewById(R.id.layout_gradfathersname);
        LmotherName = findViewById(R.id.layout_mothersname);
        LdateOfBirth = findViewById(R.id.layout_dob);
        LidCode = findViewById(R.id.layout_idCode);
        LissuedBy = findViewById(R.id.layout_issuedBy);
        LexpireDate = findViewById(R.id.layout_expireDate);
        LcustomerPhoneNumber = findViewById(R.id.layout_customerPhoneNumber);




        Signup = findViewById(R.id.btn_signup);
        firstName = findViewById(R.id.fname);
        fatherName = findViewById(R.id.father_name);
        grandFatherName = findViewById(R.id.grandFather_Name);
        motherName = findViewById(R.id.mothers_name);
        dateOfBirth = findViewById(R.id.dob);
        idCode = findViewById(R.id.idCode);
        issuedBy = findViewById(R.id.issuedBy);
        expireDate = findViewById(R.id.expireDate);
        customerPhoneNumber = findViewById(R.id.customerPhoneNumber);
        spinnerID = findViewById(R.id.spinner_IDType);
        spinnerGender = findViewById(R.id.spinner_gender);

        token = Constant.token;

        String[] idSpinner = new String[] {
                "Kebele ID", "Passport", "Driver License", "Employee ID", "Student ID", "Others"
        };

        ArrayAdapter<String> IDadapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, idSpinner);
        IDadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerID.setAdapter(IDadapter);
        identification = spinnerID.getSelectedItem().toString();

        String[] genderSpinner = new String[] {
                "Male", "Female"
        };

        expireDate.setOnTouchListener((v, event) -> {

            calendar = Calendar.getInstance();
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                // DOB picker dialog
                picker = new android.app.DatePickerDialog(Signup.this,
                        (view, year1, monthOfYear, dayOfMonth) -> {
                            calendar.set(year1, monthOfYear, dayOfMonth);
                            String dateString = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year1;
                            expireDate.setText(dateString);
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            try {
                                DOE = format.parse(dateString);
                            } catch (ParseException ignored) {
                            }
                        }, year, month, day);

                Calendar cad = Calendar.getInstance();
                cad.add(Calendar.YEAR, +2);//add two years from now
                picker.getDatePicker().setMaxDate(cad.getTimeInMillis());
                picker.show();
            }
            return false;
        });
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, genderSpinner);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);
        gender = spinnerGender.getSelectedItem().toString();

        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();

            }
        });



    }

    public void validation(){

        if (firstName == null || firstName.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter First Name", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(Lfname);
            return;
        }
        if (fatherName == null || fatherName.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Father's Name", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LfathersName);
            return;
        }
        if (grandFatherName == null || grandFatherName.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter GrandFather Name", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LgrandFatherName);
            return;
        }
        if (motherName == null || motherName.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Mother's Name", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LmotherName);
            return;
        }
        if (dateOfBirth == null || dateOfBirth.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Date of Birth", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LdateOfBirth);
            return;
        }
        if (idCode == null || idCode.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter ID code", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LidCode);
            return;
        }
        if (issuedBy == null || issuedBy.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Issued By", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LissuedBy);
            return;
        }
        if (expireDate == null || expireDate.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Expiration Date", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LexpireDate);
            return;
        }
        if (customerPhoneNumber == null || customerPhoneNumber.getText().toString().equals("")) {
            Toast.makeText(Signup.this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LcustomerPhoneNumber);

            return;
        }
        Pattern p = Pattern.compile("^[+]?[0-9]{10}$");
        Matcher m = p.matcher( customerPhoneNumber.getText());

        if (m.find()) {


        }else{
            Toast.makeText(Signup.this, "Please Enter the correct Phone Number", Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(700).repeat(0).playOn(LcustomerPhoneNumber);
            return;

        }

        Utils.showLoadingScreen(Signup.this);

        user = new User(firstName.getText().toString(), fatherName.getText().toString(), grandFatherName.getText().toString(), motherName.getText().toString(),
                dateOfBirth.getText().toString(), identification, idCode.getText().toString(), issuedBy.getText().toString(), expireDate.getText().toString(), gender, customerPhoneNumber.getText().toString(),token);
        user.save();

        if (Utils.checkConnection(Signup.this)){
            sendUsersData();
        }
        else{
            Utils.showErrorSnackBar(Signup.getRootView(), "Please connect to the internet");
            return;
        }


    }
    public void sendUsersData(){

       apiManager.createUser(user, new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Utils.closeLoadingScreen(Signup.this);
                User responseUser = response.body();
                if (response.isSuccessful() && responseUser != null) {

                    Toast.makeText(Signup.this,
                                    "success", Toast.LENGTH_LONG).show();

                    setAsRegisterd();

                    Intent intent = new Intent(Signup.this, Home.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(Signup.this,
                            "fail", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(Signup.this,
                        "Error is " + t.getMessage()
                        , Toast.LENGTH_LONG).show();
            }
        });
    }
    public void setAsRegisterd(){
        String Username = firstName.getText().toString();
        String phone = customerPhoneNumber.getText().toString();
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("Registered", true);
        editor.putString("Username", Username);
        editor.putString("phone", phone);
        editor.apply();
    }


}
