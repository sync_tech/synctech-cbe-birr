package com.example.cbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

public class AboutActivity extends AppCompatActivity {

    TextView title, versionText, aboutText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        title = findViewById(R.id.text_lift);
        versionText = findViewById(R.id.text_version);
        aboutText = findViewById(R.id.about_text);


        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            versionText.setText(String.format(Locale.US, "Version %s.%d", versionName, versionCode));
        } catch (PackageManager.NameNotFoundException ignored) {

        }
    }
}
