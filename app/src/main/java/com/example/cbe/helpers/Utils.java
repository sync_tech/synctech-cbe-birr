package com.example.cbe.helpers;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.cbe.R;
import com.google.android.material.snackbar.Snackbar;

public class Utils {
    public static Context context;
    private static Dialog alertDialog;

    public static void sendUSSD(String msg, Context context, Activity activity) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE},1);
        }
        else
        {
            context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode(msg))));

        }

    }

    public static Boolean checkConnection(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (conMgr != null) {
            netInfo = conMgr.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnected() && netInfo.isAvailable();
    }

    public static void showLoadingScreen(final AppCompatActivity appCompatActivity) {
        try {
            appCompatActivity.runOnUiThread(() -> {
                try {
                    if (alertDialog != null) {
                        try {
                            alertDialog.dismiss();
                        } catch (Exception ignored) {

                        }
                    }
                    alertDialog = new Dialog(appCompatActivity);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.loading_screen);
                    if (alertDialog.getWindow() != null) {
                        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    }
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();

                } catch (Exception ignored) {
                }
            });
        } catch (Exception ignored) {
        }
    }

    public static void closeLoadingScreen(AppCompatActivity appCompatActivity) {
        try {
            appCompatActivity.runOnUiThread(() -> {
                if (alertDialog != null) {
                    try {
                        alertDialog.dismiss();
                    } catch (Exception ignored) {
                    }
                }
            });
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    public static void showErrorSnackBar(View baseView, String message) {
        Snackbar.make(baseView, message, Snackbar.LENGTH_LONG).show();
    }


}
