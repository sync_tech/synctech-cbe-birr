package com.example.cbe.helpers;


import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

public class TypefaceUtil {

    private static Typeface DEFAULT_TYPE_FACE = Typeface.SANS_SERIF;

    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception ignored) {
        }
    }

    public static void returnToDefault() {
        try {
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField("SANS_SERIF");
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, DEFAULT_TYPE_FACE);
        } catch (Exception ignored) {
        }
    }
}
